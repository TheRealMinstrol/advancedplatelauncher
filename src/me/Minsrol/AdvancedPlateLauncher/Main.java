package me.Minsrol.AdvancedPlateLauncher;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin implements Listener {

    private ArrayList<String> launched = new ArrayList<>();

    @Override
    public void onEnable() {
        Bukkit.getServer().getLogger().info("Plugin loaded!");
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
        loadConfig();
    }

    public void loadConfig() {
        getConfig().addDefault("#1/Plate-Material", "STONE_PLATE");
        getConfig().addDefault("#1/Under-Plate-Material", "GOLD_BLOCK");
        getConfig().addDefault("#1/Pitch", Integer.valueOf(-10));
        getConfig().addDefault("#1/Launch-Multiplier", Integer.valueOf(2));
        getConfig().addDefault("#1/Play-Sound", Boolean.valueOf(true));
        getConfig().addDefault("#1/Sound", "CAT_MEOW");
        getConfig().addDefault("#1/Explosion-On-Launch", Boolean.valueOf(false));
        getConfig().addDefault("#2/Plate-Material", "GOLD_PLATE");
        getConfig().addDefault("#2/Under-Plate-Material", "IRON_BLOCK");
        getConfig().addDefault("#2/Pitch", Integer.valueOf(-10));
        getConfig().addDefault("#2/Launch-Multiplier", Integer.valueOf(2));
        getConfig().addDefault("#2/Play-Sound", Boolean.valueOf(true));
        getConfig().addDefault("#2/Sound", "CAT_MEOW");
        getConfig().addDefault("#2/Explosion-On-Launch", Boolean.valueOf(false));
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    @EventHandler
    public void moving(PlayerMoveEvent event) {
        Material plate1 = Material.getMaterial(getConfig().getString("#1/Plate-Material"));
        Material underPlateBlock1 = Material.getMaterial(getConfig().getString("1/Under-Plate-Material"));
        Sound launchSound1 = Sound.valueOf(getConfig().getString("#1/Sound"));
        final Player p = event.getPlayer();
        if (p.getLocation().getBlock().getType().equals(plate1)) {
            if (p.getLocation().add(0, -1, 0).getBlock().getType().equals(underPlateBlock1)) {
                Location launch = p.getLocation().clone();
                launch.add(0, 10, 0);
                launch.setPitch(getConfig().getInt("#1/Pitch"));
                p.setVelocity(launch.getDirection().multiply(getConfig().getInt("#1/Launch-Multiplier")));
                launched.add(p.getName());
                if (getConfig().getBoolean("#1/Play-Sound")) {
                    p.playSound(p.getLocation(), launchSound1, 10, 5);
                }
                if (getConfig().getBoolean("#1/Explosion-On-Launch")) {
                    Bukkit.getWorld(p.getWorld().getName()).createExplosion(p.getLocation(), 0F);
                }
            }
        }
        Material plate2 = Material.getMaterial(getConfig().getString("#2/Plate-Material"));
        Material underPlateBlock2 = Material.getMaterial(getConfig().getString("#2/Under-Plate-Material"));
        Sound launchSound2 = Sound.valueOf(getConfig().getString("#2/Sound"));
        if (p.getLocation().getBlock().getType().equals(plate2)) {
            if (p.getLocation().add(0, -1, 0).getBlock().getType().equals(underPlateBlock2)) {
                Location launch = p.getLocation().clone();
                launch.add(0, 10, 0);
                launch.setPitch(getConfig().getInt("#2/Pitch"));
                p.setVelocity(launch.getDirection().multiply(getConfig().getInt("#2/Launch-Multiplier")));
                launched.add(p.getName());
                if (getConfig().getBoolean("#2/Play-Sound")) {
                    p.playSound(p.getLocation(), launchSound2, 10, 5);
                }
                if (getConfig().getBoolean("#2/Explosion-On-Launch")) {
                    Bukkit.getWorld(p.getWorld().getName()).createExplosion(p.getLocation(), 0F);
                }
            }
        }
    }
}
